<?php //Start building your awesome child theme functions


function my_scripts_method() {
	wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/custom-script.js');


}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

// Remove prices everywhere
 
//add_filter( 'woocommerce_variable_sale_price_html', 'businessbloomer_remove_prices', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'businessbloomer_remove_prices', 10, 2 );
//add_filter( 'woocommerce_get_price_html', 'businessbloomer_remove_prices', 10, 2 );
 
function businessbloomer_remove_prices( $price, $product ) {
$price = '';
return $price;
}

function addPriceSuffix($format, $currency_pos) {
	switch ( $currency_pos ) {
		case 'left' :
			$currency = get_woocommerce_currency();
			$format = '%1$s%2$s&nbsp;' . '<span class="currency-nzd">' .$currency. '</span>';
		break;
	}
 
	return $format;
}
 
add_action('woocommerce_price_format', 'addPriceSuffix', 1, 2);